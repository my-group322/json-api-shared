package jsonapiresources

import (
	"encoding/json"

	"github.com/pkg/errors"
)


type Key struct {
	ID   string       `json:"id"`
	Type ResourceType `json:"type"`
}

func (r *Key) GetKey() Key {
	return *r
}

func (r Key) GetKeyP() *Key {
	return &r
}

func (r Key) AsRelation() *Relation {
	return &Relation{
		Data: r.GetKeyP(),
	}
}

type Resource interface {
	GetKey() Key
}

type Included struct {
	includes map[Key]json.RawMessage
}

func (c *Included) Add(res Resource) {
	if c.includes == nil {
		c.includes = make(map[Key]json.RawMessage)
	}

	data, err := json.Marshal(res)
	if err != nil {
		panic(errors.Wrap(err, "failed to add into includes"))
	}

	c.includes[res.GetKey()] = data
}

func (c Included) MarshalJSON() ([]byte, error) {
	entries := make([]json.RawMessage, 0, len(c.includes))
	for _, value := range c.includes {
		entries = append(entries, value)
	}

	return json.Marshal(entries)
}

func (c *Included) UnmarshalJSON(data []byte) error {
	var keys []Key
	err := json.Unmarshal(data, &keys)
	if err != nil {
		return errors.Wrap(err, "failed to unmarshal keys for include")
	}

	var entries []json.RawMessage
	err = json.Unmarshal(data, &entries)
	if err != nil {
		return errors.Wrap(err, "failed to unmarshal entries for include")
	}

	c.includes = make(map[Key]json.RawMessage)
	for i := range keys {
		c.includes[keys[i]] = entries[i]
	}

	return nil
}

type Relation struct {
	Data *Key `json:"data,omitempty"`
}

type RelationCollection struct {
	Data []Key `json:"data"`
}

func (r RelationCollection) MarshalJSON() ([]byte, error) {
	if r.Data == nil {
		r.Data = []Key{}
	}

	type temp RelationCollection
	return json.Marshal(temp(r))
}

type ResourceType string
